/**
 * Base64, Base64URL, compression and JSON utility classes.
 *
 * @version $version$ ($version-date$)
 */
package com.nimbusds.jose.util;
